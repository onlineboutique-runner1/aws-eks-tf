variable "aws_access_key" {
  description = "The AWS access key."
}

vairable "region" {
  
}

variable "aws_secret_key" {
  description = "The AWS secret key."
}

variable "bucket_name" {
  description = "The name of the S3 bucket."
}

variable "dynamodb_table_name" {
  description = "The name of the DynamoDB table."
}
