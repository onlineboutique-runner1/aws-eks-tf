provider "aws" {
  region     = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

resource "aws_s3_bucket" "online23-23" {
  bucket = var.bucket_name
  # acl    = "private"  # Set the bucket ACL (Access Control List) - Optional
}

resource "aws_dynamodb_table" "s3db" {
  name           = var.dynamodb_table_name
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

terraform {
  backend "s3" {
    bucket         = var.bucket_name  # Specify the name of the S3 bucket you created
    key            = "terraform/state.tfstate"  # Specify the path to the state file within the bucket
    region         = "eu-north-1"  # Specify the AWS region where the S3 bucket is located
    dynamodb_table = var.dynamodb_table_name  # Specify the name of the DynamoDB table you created
    encrypt        = true  # Optionally enable encryption for the state file
  }
}
